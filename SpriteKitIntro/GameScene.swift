//
//  GameScene.swift
//  SpriteKitIntro
//
//  Created by Vivek Batra on 2019-09-30.
//  Copyright © 2019 student. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {

    // This is our constructor
    override func didMove(to view: SKView) {
        print("Hello world")
        
        print("Screen Size (w, h): \(size.width), \(size.height)")
    }
    
    // This is update frame function which runs once a frame
    override func update(_ currentTime: TimeInterval) {
       
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Fingure touches screen")
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("fingure lefted from screen")
        
        // getting x y position
        let locationTouched = touches.first;
        if (locationTouched == nil) {
            return
        }
        
        let mousePosition  = locationTouched!.location(in: self)
        print("x: \(mousePosition.x)");
        print("y: \(mousePosition.y)")
    }
    
    
}
